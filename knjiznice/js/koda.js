
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var osebe = [
    {
        "ime": "Miha",
        "priimek": "Pozenel",
        "rojen": "1980-05-22",
        "teza": ["65", "66","67", "69","71"],
        "visina": ["165", "165","165", "166","165"],
        "datumMerjenja": ["2005", "2010","2011", "2012","2013"],
    },
    {
        "ime": "Gorazd",
        "priimek": "Gabrsek",
        "rojen": "1950-01-02",
        "teza": ["110", "105","99", "91","85"],
        "visina": ["188", "188","188", "187","186"],
        "datumMerjenja": ["2010", "2012","2014", "2016","2018"],
    },
    {
        "ime": "Maja",
        "priimek": "Bozoran",
        "rojen": "1966-02-13",
        "teza": ["45", "46","50", "50","53"],
        "visina": ["165", "165","165", "165","165"],
        "datumMerjenja": ["1990", "1992","1993", "1994","1995"],
    }
];

var generirano = false;

function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}

function generirajPodatke() {
 
 if(generirano == false) {
 	generirano = true;
 	
	for(var i = 0; i < 3; i++){
		kreirajEHRzaBolnika(true,i);
	  }
 }
  
}

function kreirajEHRzaBolnika(preset,stOsebe) {
    
    console.log("---Generiranje EHR--- ");
    
	sessionId = getSessionId();
	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
	var datumRojstva = $("#kreirajDatumRojstva").val() + "T00:00:00.000Z";
	
	if(preset == true){
		ime = osebe[stOsebe].ime;
		priimek = osebe[stOsebe].priimek;
		datumRojstva = osebe[stOsebe].rojen;
	}
	
	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		       	console.log("ID: " + ehrId );
	
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
		                    $("#preberiEHRid").val(ehrId);
		                }
		                
		                $("#preberiObstojeciVitalniZnak").append($('<option>',{
		                    value: ehrId,
		                    text: ime
		                }));
		                
		                 $("#preberiEhrIdZaVitalneZnake").append($('<option>',{
		                    value: ehrId,
		                    text: ime
		                }));
		                
		            },
		            error: function(err) {
		            /*	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");*/
                    
                    console.log("error creating ehrID");
		            }
		        });
		        for(var i = 0; i < 5; i++){
		    	  dodajMeritveVitalnihZnakov(true,stOsebe,data.ehrId,i);
		        }
		    }
		});
	}
}

function dodajMeritveVitalnihZnakov(preset,stOsebe,id,nr) {
	sessionId = getSessionId();
	console.log("---Dodajanje Meritev--- ");
	var ehrId = $("#dodajVitalnoEHR").val();
	var datumInUra = $("#dodajVitalnoDatumInUra").val();
	var telesnaVisina = $("#dodajVitalnoTelesnaVisina").val();
	var telesnaTeza = $("#dodajVitalnoTelesnaTeza").val();
	
	if(preset){
		ehrId = id;
		datumInUra = osebe[stOsebe].datumMerjenja[nr];
		telesnaVisina = osebe[stOsebe].visina[nr];
		telesnaTeza = osebe[stOsebe].teza[nr];
	}
	
	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
              // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    commiter: ''
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        $("#dodajMeritveVitalnihZnakovSporocilo").html(
              "<span class='obvestilo label label-success fade-in'>" +
              res.meta.href + ".</span>");
		    },
		    error: function(err) {
		    	$("#dodajMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
	}
	

	console.log("ID: " + ehrId + " height: " + telesnaVisina + " weight: " + telesnaTeza );
	
} 

function sendNoDataError(){
		$("#preberiMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-warning fade-in'>" +
             "Ni podatkov!</span>");
}

function calcBMI(weight,height){
	var bmi =  ((weight / (height*height))*10000);
	return Math.round(bmi*100)/100
}

function preberiMeritveVitalnihZnakov() {
	sessionId = getSessionId();
	var ehrId = $("#meritveVitalnihZnakovEHRid").val();
	var tip = $("#preberiTipZaVitalneZnake").val();
	if (!ehrId || ehrId.trim().length == 0 || !tip || tip.trim().length == 0) {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
			    
			    if (tip == "telesna teža") {
			          console.log("---Showing Weight---");
					$.ajax({
					    url: baseUrl + "/view/" + ehrId + "/" + "weight",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					     if (res.length > 0) {
                                    var weight = [];
                                    var time = [];
                                    var podatki = [];
                                    var st = 0;
                                    for (var i in res) {
                                        var item = res[i];
                                            weight[st] = item.weight;
                                            time[st] = item.time;
                                    		st++;
                                    }
                                    podatki = {
                                    	x:time,
                                    	y:weight
                                    }
                                    drawGraph(podatki,"weight");  
                                }  else{sendNoDataError();}
					    }
					   
					});
				}else if (tip == "telesna višina") {
				    console.log("---Showing Height---");
					$.ajax({
					    url: baseUrl + "/view/" + ehrId + "/" + "height",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
                                if (res.length > 0) {
                                    var time = [];
                                    var height = [];
                                    var podatki = [];
                                    var st = 0;
                                    for (var i in res) {
                                        var item = res[i];
                                            time[st] = item.time;
                                            height[st] = item.height;
                                    		st++;
                                    }
                                	podatki = {
                                		x:time,
                                		y:height
                                	}
                                    drawGraph(podatki,"height");  
                                }else {sendNoDataError()}
                        }
					   
					});
				
					
				
					
				}else if(tip == "BMI"){
					 console.log("---Showing BMI---");
					$.ajax({
					    url: baseUrl + "/view/" + ehrId + "/" + "weight",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					     if (res.length > 0) {
                                    var weight = [];
                                    var st = 0;
                                    for (var i in res) {
                                        var item = res[i];
                                            weight[st] = item.weight;
                                    		st++;
                                    }
                          $.ajax({
							    url: baseUrl + "/view/" + ehrId + "/" + "height",
							    type: 'GET',
							    headers: {"Ehr-Session": sessionId},
							    podatki: weight,
							    success: function (res) {
							     if (res.length > 0) {
		                                    var height = [];
		                                    var time = [];
		                                    var st = 0;
		                                    for (var i in res) {
		                                        var item = res[i];
		                                            height[st] = item.height;
		                                            time[st] = item.time;
		                                    		st++;
		                                    }
		                                    // console.log(this.podatki);
		                                    // console.log(height);
		                                    // console.log(time);
		                                     var bmi = [];
		                                     for(var i = 0; i < time.length; i++){
		                                     	bmi[i] = calcBMI(this.podatki[i],height[i]);
		                                     }
		                                     // console.log(bmi);
		                                     var arr = {
		                                     	x:time,
		                                     	y:bmi
		                                     }
		                                     
		                                     $.ajax({
												url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
												type: 'GET',
												podatki: arr,
												headers: {"Ehr-Session": sessionId},
										    	success: function (data) {
													var party = data.party;
													var letoRojstva = parseInt(data.party.dateOfBirth.substr(0,4));
												//	console.log(this.podatki);
			                                     	drawBMI(this.podatki,letoRojstva);
		                                		  }
		                                    });     
		                                 
		                                     
		                         }else {sendNoDataError()}
							    },
							});          
                         }else {sendNoDataError()}
					    },
					});
				}
	    	},
	    	error: function(err) {
	    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
	    	}
		});
	}
}

function drawBMI(podatki,letoRojstva){
	//	console.log(podatki);
	//	console.log(letoRojstva);
	var starosti = [];
	for(var i = 0; i < podatki.x.length; i++){
		var letoMeritve = parseInt(podatki.x[i].substr(0,4));
		var starost = letoMeritve - letoRojstva;
		starosti[i] = starost;
	}
	console.log(starosti);
	var bmi = {
		x:starosti,
		y:podatki.y
	}
	
//	console.log(bmi);
	drawGraph(bmi,"BMI");
}

function drawGraph(podatki,type){

	console.log("type: " + type );

	var layout;
	var data = [podatki];
	
	if(type == "BMI"){
		
		layout = {
		 autosize: true,
		  margin: {
		    l: 40,
		    r: 0,
		    b: 40,
		    t: 20,
		    pad: 4
		  },
		  
		  xaxis : {
		  	title: "age"
		  },
		  
		  yaxis : {
		  	title: "BMI"
		  }
		  
		};
		podatki.name = "your BMI";
			
		var bmiMin = [];
		var bmiMax = [];
		
		for(var i = 0; i < podatki.x.length; i++){
			bmiMin[i] = 18.5;
			bmiMax[i] = 25;
		}
		
		var avgBmiMin = {
			x:podatki.x,
			y:bmiMin,
			name:"minimum healthy BMI"
		}
			var avgBmiMax = {
			x:podatki.x,
			y:bmiMax,
			name:"maximum healthy BMI"
		}
		
		data = [podatki,avgBmiMax,avgBmiMin];
		
	}else if(type == "height"){
		
			layout = {
	 autosize: true,
	  margin: {
	    l: 60,
	    r: 0,
	    b: 40,
	    t: 20,
	    pad: 4
	  },
	  
	  xaxis : {
	  	title: "year"
	  },
	  
	  yaxis : {
	  	title: "height"
	  }
	  
	};
	podatki.name = "your height";
		
	}else if(type == "weight"){
			layout = {
	 autosize: true,
	  margin: {
	    l: 40,
	    r: 0,
	    b: 40,
	    t: 20,
	    pad: 4
	  },
	  
	  xaxis : {
	  	title: "year"
	  },
	  
	  yaxis : {
	  	title: "weight"
	  }
	  
	};
	podatki.name = "your weight";
	}

	Plotly.newPlot('rezultatMeritveVitalnihZnakov', data,layout);
	
	
	if(type == "BMI"){
		checkBMI(podatki);
	}
}


function checkBMI(podatki){
	
	var bmi = podatki.y[0];
	if(bmi > 25.0){
		
		 var ask = window.confirm("Your BMI is too high, would you like to see gyms near Ljubljana");
		 if (ask) 
			window.open('map.html');
	}
}

$(document).ready(function() {

  /**
   * Napolni testne vrednosti (ime, priimek in datum rojstva) pri kreiranju
   * EHR zapisa za novega bolnika, ko uporabnik izbere vrednost iz
   * padajočega menuja (npr. Pujsa Pepa).
   */
  $('#preberiPredlogoBolnika').change(function() {
    $("#kreirajSporocilo").html("");
    var podatki = $(this).val().split(",");
    $("#kreirajIme").val(podatki[0]);
    $("#kreirajPriimek").val(podatki[1]);
    $("#kreirajDatumRojstva").val(podatki[2]);
  });

  /**
   * Napolni testni EHR ID pri prebiranju EHR zapisa obstoječega bolnika,
   * ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Dejan Lavbič, Pujsa Pepa, Ata Smrk)
   */
	$('#preberiObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		$("#preberiEHRid").val($(this).val());
	});

  /**
   * Napolni testne vrednosti (EHR ID, datum in ura, telesna višina,
   * telesna teža, telesna temperatura, sistolični in diastolični krvni tlak,
   * nasičenost krvi s kisikom in merilec) pri vnosu meritve vitalnih znakov
   * bolnika, ko uporabnik izbere vrednosti iz padajočega menuja (npr. Ata Smrk)
   */
	$('#preberiObstojeciVitalniZnak').change(function() {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("");
		var podatki = $(this).val().split("|");
		$("#dodajVitalnoEHR").val(podatki[0]);
		$("#dodajVitalnoDatumInUra").val(podatki[1]);
		$("#dodajVitalnoTelesnaVisina").val(podatki[2]);
		$("#dodajVitalnoTelesnaTeza").val(podatki[3]);
		$("#dodajVitalnoTelesnaTemperatura").val(podatki[4]);
		$("#dodajVitalnoKrvniTlakSistolicni").val(podatki[5]);
		$("#dodajVitalnoKrvniTlakDiastolicni").val(podatki[6]);
		$("#dodajVitalnoNasicenostKrviSKisikom").val(podatki[7]);
		$("#dodajVitalnoMerilec").val(podatki[8]);
	});

  /**
   * Napolni testni EHR ID pri pregledu meritev vitalnih znakov obstoječega
   * bolnika, ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Ata Smrk, Pujsa Pepa)
   */
	$('#preberiEhrIdZaVitalneZnake').change(function() {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("");
		$("#rezultatMeritveVitalnihZnakov").html("");
		$("#meritveVitalnihZnakovEHRid").val($(this).val());
	});

});